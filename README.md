The Community branch of the MMMerge project - Might and Magic Merge, also known as the World of Enroth.
Check the thread here: https://www.celestialheavens.com/forum/10/16657

# WILL ONLY WORK WITH MERGE VERSION 08.10.2019 !

### Installation instructions -

0. Install the Merge - by installing the original game, then the merge up to the compatible version mentioned (if the newest one by Rodril is newer, please send me an email to templayer@seznam.cz and I will update the community branch) and the newest GrayFace patch on top of that (link is in the first post in the thread)
1. Delete the script folder (we had problems with rogue lua files before...)
2. Copy paste the folders from GitLab into your installation directory, replacing existing (except for the script folder mentioned in the previous point... that one will be added instead of replaced)
3. Use and configure any optional content in the "Changes from the Vanilla (Base / non-community branch) Merge" (i.e. following) section
4. Play.

### Changes from the Vanilla (Base / non-community branch) Merge -
1. Icon - there is an icon contained within the branch, made by Templayer out of the Dracolich assets, which were made by GDSpectra, Jamesx and Templayer. Should be compatible with Windows XP, but still includes a 256x256 image variant for systems that support such high-res icons. (also GitLab is telling me that it only contains the 256x256, which is false...)
